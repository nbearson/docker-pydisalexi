# A Dockerfile to provide all the requirements for running pyDisALEXI
FROM continuumio/anaconda

RUN apt-get update
RUN apt-get -y upgrade

RUN apt-get install -y libopenjpeg5 libopenjpeg-dev
RUN apt-get install -y vim

RUN conda update -y --prefix /opt/conda anaconda
RUN conda install -y gdal
RUN conda install -y -c conda-forge pyhdf

RUN pip install utm
RUN pip install pydap

RUN conda install -y pyproj
RUN apt-get install -y gcc
RUN conda install -y -c conda-forge pygrib=2.0.1
#RUN pip install pygrib

RUN conda install -y -c conda-forge keyring
RUN conda install -y -c conda-forge joblib

# needs libjpeg.so.8
RUN ln -s /opt/conda/lib/libjpeg.so.9 /opt/conda/lib/libjpeg.so.8

# some extra shell niceties and fluff for poking about
RUN apt-get install -y vim

RUN echo 'alias ls="ls --color=auto"' >> ~/.bashrc && \
    echo 'alias ll="ls -lGh $@"' >> ~/.bashrc

# additions for projectMASlai

RUN pip install scons wget simplejson pymodis
RUN apt-get install -y libgctp-dev
# RUN apt-get install libhe5-hdfeos0 libhe5-hdfeos-dev
RUN conda config --add channels conda-forge
RUN conda install hdfeos2
# note there's also a hdfeos5 - I think that's the hdf5 one though?

# processlai has hardcoded versions?
# ln -s /opt/conda/lib/libgeos-3.5.0.so /opt/conda/lib/libgeos-3.4.2.so
# ln -s /opt/conda/lib/libnetcdf.so /opt/conda/lib/libnetcdf.so.7
# ln -s /opt/conda/lib/libkea.so /opt/conda/lib/libkea.so.1.4.5
# ln -s /opt/conda/lib/libhdf5_cpp.so /opt/conda/lib/libhdf5_cpp.so.10